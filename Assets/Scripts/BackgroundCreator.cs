﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundCreator : MonoBehaviour
{
    public GameObject wall;
    public static float NORMAL_CHANCE = 5.0f; //out of 10
    public GameObject wallWithWholes;
    public static float WHOLES_CHANCE = 4.0f; //out of 10
    public GameObject wallWithWindow;
    public static float WINDOW_CHANCE = 0.0f; //out of 10 - deliberetly not using wall with one window
    public GameObject wallWithWindows;
    public static float WINDOWS_CHANCE = 1.0f; //out of 10 
    public static int MIN_X = -6;
    public static int MAX_X = 6;
    public static int SIZE = 4;
    public static int MIN_Y = -46;
    public static int MAX_Y = 10;

    // Start is called before the first frame update
    void Start()
    {
        for (int y = MIN_Y; y <= MAX_Y; y += SIZE)
        {
            for (int x = MIN_X; x <= MAX_X; x += SIZE)
            {
                float rand = Random.Range(0, 10);
                if (rand < NORMAL_CHANCE)
                {
                    GameObject go = Instantiate(wall, new Vector3(x, y, 0), transform.rotation);
                    go.transform.SetParent(transform);
                }
                else if(rand < NORMAL_CHANCE + WHOLES_CHANCE)
                {
                    GameObject go = Instantiate(wallWithWholes, new Vector3(x, y, 0), transform.rotation);
                    go.transform.SetParent(transform);
                }
                else if(rand < NORMAL_CHANCE + WHOLES_CHANCE + WINDOWS_CHANCE)
                {
                    GameObject go = Instantiate(wallWithWindows, new Vector3(x, y, 0), transform.rotation);
                    go.transform.SetParent(transform);
                }
                else
                {
                    GameObject go = Instantiate(wallWithWindow, new Vector3(x, y, 0), transform.rotation);
                    go.transform.SetParent(transform);
                }
            }
        }
    }
}
