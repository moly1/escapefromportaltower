﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    public static float RANGE = 20.5f;
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(-1,1);
        if (speed < 0)
        {
            speed = Random.Range(-0.05f, -0.01f);
        }
        else
        {
            speed = Random.Range(0.01f, 0.05f);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(transform.position.x > RANGE)
        {
            transform.position = new Vector3(-RANGE + 1, transform.position.y, 0);
        }
        else if (transform.position.x < -RANGE)
        {
            transform.position = new Vector3(RANGE - 1, transform.position.y, 0);
        }
        else
        {
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, 0);
        }
    }
}
