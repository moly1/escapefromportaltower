﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Rigidbody2D body;
    private SpriteRenderer sprite;
    private float direction;
    public static float CHARGING_SPEED = 10;

    private void Start()
    {
        body = transform.GetComponentInChildren<Rigidbody2D>();
        sprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Charge(collision.transform.position);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Stop();
        }
    }

    private void Charge(Vector3 player)
    {
        direction = Mathf.Sign(player.x - transform.GetChild(0).position.x);
        if (body != null) { body.velocity = new Vector2(CHARGING_SPEED * direction, 0); }
        if (sprite != null) { sprite.color = new Color(1, 0, 0, 1); }
    }

    private void Stop()
    {
        sprite.color = new Color(1, 1, 1, 1);
    }
}
