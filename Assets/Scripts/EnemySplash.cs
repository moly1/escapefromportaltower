﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySplash : MonoBehaviour
{
    private AudioSource audio;
    [SerializeField] private AudioClip smash;

    private void Start()
    {
        audio = transform.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Platform")
        {
            if(audio!=null)
            audio.PlayOneShot(smash);
        }
    }
}
