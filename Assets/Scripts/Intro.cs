﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    [SerializeField] private Text[] intro;
    private int textDisplayed = 0;

    private void Start()
    {
        foreach (Text t in intro) { t.enabled = false; }
        intro[textDisplayed].enabled = true;
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void Next()
    {
        textDisplayed += 1;
        if (textDisplayed < intro.Length)
        {
            foreach (Text t in intro) { t.enabled = false; }
            intro[textDisplayed].enabled = true;
        }
        else
        {
            LoadGame();
        }
    }
}
