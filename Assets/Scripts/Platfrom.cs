﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platfrom : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector2 size = transform.GetComponent<SpriteRenderer>().size;
        BoxCollider2D bc = transform.GetComponent<BoxCollider2D>();
        bc.size = size;
    }
}
