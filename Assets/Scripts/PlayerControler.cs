﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public static float PLAYER_SPEED = 5f;
    public static float MAX_FALLING_SPEED = 10f;
    public static float PLAYER_ACCELERATION = 1.2f;
    public static float JUMP_VELOCITY = 10f;
    public static int PRE_JUMP_TIME = 4;
    public static int JUMP_TIME = 10;
    public static int POST_JUMP_TIME = 20;
    public static int HIT_TIME = 10;
    public static int IMMORTAL_TIME = 30;
    public static int BACK_TO_MORTAL_TIME = 40;
    public static int STEP_TIME = 10;
    private Rigidbody2D body;
    private Animator anim;

    private SpriteRenderer sprite;
    private bool grounded;
    private bool jumping;
    private bool alive;
    private bool mortal;
    private int hp;
    private AudioSource audio;
    private bool steping;
    [SerializeField] private AudioClip step;
    [SerializeField] private AudioClip jump;
    [SerializeField] private AudioClip hit;


    private void Start()
    {
        GameMenager.menager.RegisterPlayer(this);
        body = transform.GetComponentInParent<Rigidbody2D>();
        anim = transform.GetComponentInParent<Animator>();
        sprite = transform.parent.GetComponent<SpriteRenderer>();
        audio = transform.parent.GetComponent<AudioSource>();
        grounded = false;
        jumping = false;
        alive = true;
        mortal = true;
        hp = 3;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Platform")
        {
            if (!jumping)
            {
                grounded = true;
            }
            anim.SetFloat("FallingSpeed", 0);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Platform")
        {
            if (!jumping)
            {
                grounded = true;
            }
            anim.SetFloat("FallingSpeed", 0);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Platform")
        {
            grounded = false;
        }
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Move(-1);
            TurnAround(-1);
            anim.SetBool("Running", true);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Move(1);
            TurnAround(1);
            anim.SetBool("Running", true);
        }
        else
        {
            Move(0);
            anim.SetBool("Running", false);
        }
        if (!grounded && !jumping)
        {
            if (Input.GetKey(KeyCode.S))
            {
                Fall(MAX_FALLING_SPEED * 1.5f);
                anim.SetFloat("FallingSpeed", 1.5f);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                Fall(MAX_FALLING_SPEED * 0.5f);
                anim.SetFloat("FallingSpeed", 0.5f);
            }
            else
            {
                Fall(MAX_FALLING_SPEED);
                anim.SetFloat("FallingSpeed", 1f);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (!jumping)
                {
                    StartCoroutine("Jump");
                }
            }
        }

    }

    private void Move(int direction)
    {
        if (alive)
        {
            if (direction == 0)
            {
                body.velocity = new Vector2(body.velocity.x * 3f / 4f, body.velocity.y);
            }
            else
            {
                if (Mathf.Abs(body.velocity.x) > PLAYER_SPEED)
                {
                    body.velocity = new Vector2(direction * PLAYER_SPEED, body.velocity.y);
                }
                else
                {
                    body.velocity = new Vector2(body.velocity.x + PLAYER_ACCELERATION * direction, body.velocity.y);
                }
                if (!steping && grounded)
                {
                    StartCoroutine("Step");
                }
            }
        }
    }

    private IEnumerator Step()
    {
        steping = true;
        audio.PlayOneShot(step);
        for(int i = 0; i < STEP_TIME; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        steping = false;
    }

    private void TurnAround(int direction)
    {
        if (alive)
        {
            if (direction == -1)
            {
                transform.parent.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            }
            else
            {
                transform.parent.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            }
        }
    }

    private void Fall(float maxFallingSpeed)
    {
        if (Mathf.Abs(body.velocity.y) > maxFallingSpeed)
        {
            body.velocity = new Vector2(body.velocity.x, -maxFallingSpeed);
        }
    }

    private IEnumerator Jump()
    {
        if (alive)
        {
            audio.PlayOneShot(jump);
            grounded = false;
            jumping = true;
            anim.SetBool("Jumping", true);
            for (int i = 0; i < PRE_JUMP_TIME; i++)
            {
                yield return new WaitForFixedUpdate();
            }
            for (int i = 0; i < JUMP_TIME; i++)
            {
                body.velocity = new Vector2(body.velocity.x, JUMP_VELOCITY);
                yield return new WaitForFixedUpdate();
            }
            for (int i = 0; i < POST_JUMP_TIME; i++)
            {
                yield return new WaitForFixedUpdate();
            }
            jumping = false;
            anim.SetBool("Jumping", false);
        }
    }

    public void TakeDemage(int demage)
    {
        if (mortal)
        {
            if (hp > 0) { hp -= demage; }
            if (hp <= 0)
            {
                if (alive)
                {
                    GameMenager.menager.PlayerDied();
                }
            }
            else
            {
                audio.PlayOneShot(hit);
                StartCoroutine("Immortal");
            }
        }
    }

    private IEnumerator Immortal()
    {
        mortal = false;
        sprite.color = new Color(1, 0, 0, 1);
        for(int i = 0; i < HIT_TIME; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        sprite.color = new Color(1, 1, 1, 0.5f);
        for (int i = 0; i < IMMORTAL_TIME; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        sprite.color = new Color(1, 1, 1, 1);
        for (int i = 0; i < BACK_TO_MORTAL_TIME/4; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        sprite.color = new Color(1, 1, 1, 0.5f);
        for (int i = 0; i < BACK_TO_MORTAL_TIME / 4; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        sprite.color = new Color(1, 1, 1, 1);
        for (int i = 0; i < BACK_TO_MORTAL_TIME / 4; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        sprite.color = new Color(1, 1, 1, 0.5f);
        for (int i = 0; i < BACK_TO_MORTAL_TIME / 4; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        mortal = true;
        sprite.color = new Color(1, 1, 1, 1);
    }

    public int HP()
    {
        return hp;
    }

    public void ResettHP()
    {
        hp = 3;
    }

    public void Die()
    {
        alive = false;
        body.velocity = new Vector2(0, body.velocity.y);
        anim.SetBool("Alive", false);

    }

    public void Resurect()
    {
        alive = true;
        anim.SetBool("Alive", true);
        hp = 3;
    }

    public void TeleportTo(Vector3 position)
    {
        transform.parent.position = position;
        body.velocity = new Vector2(0, 0);
    }
}
