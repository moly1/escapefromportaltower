﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DifficultyLevel { normal, hard, insane };

public class PortalControler : MonoBehaviour
{
    public static float PORTAL_SPEED = 4f;
    public static float PORTAL_ACCELERATION=1;
    private Rigidbody2D body;

    private void Start()
    {
        body = transform.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && GameMenager.menager.isActive)
        {
            if (transform.position.x < 2)
            {
                GameMenager.menager.NextLevel(DifficultyLevel.normal);
            }
            else if (transform.position.x > 0)
            {
                GameMenager.menager.NextLevel(DifficultyLevel.hard);
            }
        }
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Move(-1);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Move(1);
        }
        else
        {
            Move(0);
        }
    }

    private void Move(int direction)
    {
        if (direction == 0)
        {
            body.velocity = new Vector2(body.velocity.x * 3f/4f , body.velocity.y);
        }
        else
        {
            if(Mathf.Abs(body.velocity.x) > PORTAL_SPEED)
            {
                body.velocity = new Vector2 (direction * PORTAL_SPEED, body.velocity.y);
            }
            else
            {
                body.velocity = new Vector2(body.velocity.x + PORTAL_ACCELERATION * direction, body.velocity.y);
            }
        }
    }

}
