﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenager : MonoBehaviour
{

    public RawImage[] lives;
    public Text score;
    public Canvas RestartMenu;
    public Canvas InGameUi;
    public Canvas VictoryScreen;
    public Text easy;
    public Text hard;
    public Text died;
    public Text endScore;
    public Text grade;

    void Start()
    {
        RestartMenu.enabled = false;
        VictoryScreen.enabled = false;
        GameMenager.menager.RegisterUi(this);
    }

    public void SetHP( int hp)
    {
        if (hp <= 0) { hp = 0; }
        for (int i = 0; i < lives.Length && i < hp; i++)
        {
            lives[i].color = new Color(1, 1, 1, 1);
        }

        for ( int i = hp; i<lives.Length; i++)
        {
            lives[i].color = new Color(0, 0, 0, 1);
        }
    }

    public void UpdateScore(int score)
    {
        this.score.text = score.ToString();
    }

    public void EnableRestartMenu()
    {
        RestartMenu.enabled = true;
    }

    public void DisablbeRestartMenu()
    {
        RestartMenu.enabled = false;
    }

    public void ShowVictoryScreen(int deaths, int score, int hardLevels, int easyLevels)
    {
        InGameUi.enabled = false;
        VictoryScreen.enabled = true;
        died.text = deaths.ToString() + " times";
        endScore.text = score.ToString();
        hard.text = hardLevels.ToString();
        easy.text = easyLevels.ToString();
        if (score < 20000)
        {
            grade.text = "D-";
        }
        else if(score<40000)
        {
            grade.text = "D";
        }
        else if (score < 60000)
        {
            grade.text = "C";
        }
        else if (score < 90000)
        {
            grade.text = "B";
        }
        else if (score < 150000)
        {
            grade.text = "A";
        }
        else if (score < 170000)
        {
            grade.text = "S";
        }
        else if (score <= 190000)
        {
            grade.text = "SS";
        }
        else if (score > 190000)
        {
            grade.text = "SSS";
        }
    }

}
